package com.iko.android.wallet;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;


import java.util.ArrayList;


/**
 * A simple {@link Fragment} subclass.
 */
public class HomeFragment extends Fragment {
private RecyclerView mRecyclerView;
private RecyclerView.Adapter mAdapter;
private RecyclerView.LayoutManager mLayoutManager;

    public HomeFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View rootView = inflater.inflate(R.layout.fragment_home, container, false);


        ArrayList<ExampleItem> itemList = new ArrayList<>();
        itemList.add(new ExampleItem(R.drawable.ic_monetization_on_black_24dp,"10000","14-10-2018 : 14:23 WIB"));
        itemList.add(new ExampleItem(R.drawable.ic_monetization_on_black_24dp,"25000","14-10-2018 : 14:53 WIB"));
        itemList.add(new ExampleItem(R.drawable.ic_monetization_on_black_24dp,"43000","15-10-2018 : 17:53 WIB"));
        itemList.add(new ExampleItem(R.drawable.ic_monetization_on_black_24dp,"20000","16-10-2018 : 18:23 WIB"));
        itemList.add(new ExampleItem(R.drawable.ic_monetization_on_black_24dp,"69000","17-10-2018 : 20:13 WIB"));
        itemList.add(new ExampleItem(R.drawable.ic_monetization_on_black_24dp,"56000","18-10-2018 : 16:33 WIB"));

        mRecyclerView = (RecyclerView) rootView.findViewById(R.id.recyclerViewHome);
        mRecyclerView.setHasFixedSize(true);

        mLayoutManager = new LinearLayoutManager(getActivity());
        mAdapter = new ExampleAdapter(itemList);

        mRecyclerView.setLayoutManager(mLayoutManager);
        mRecyclerView.setAdapter(mAdapter);
        mAdapter.notifyDataSetChanged();




        Log.d("tes",itemList.get(2).getmText1());
        mRecyclerView.setItemAnimator(new DefaultItemAnimator());


        return rootView;
    }

}
