package com.iko.android.wallet;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;

public class CartAdapter extends RecyclerView.Adapter<CartAdapter.Item_TopUp_History_ViewHolder> {
    public ArrayList<Order> mItemList;

    private OnItemClickListener mListener;

    public interface OnItemClickListener {
        void onItemClick(int position);
    }

    public void setOnItemClickListener(OnItemClickListener listener ){
        mListener = listener;

    }




    public static class Item_TopUp_History_ViewHolder extends RecyclerView.ViewHolder{
       // public ImageView mImageView;
        public TextView mTextView1;
        public TextView mTextView2;

        public Item_TopUp_History_ViewHolder(@NonNull View itemView, final OnItemClickListener listener) {
            super(itemView);
           // mImageView=itemView.findViewById(R.id.imageView);
            mTextView1=itemView.findViewById(R.id.textView);
            mTextView2=itemView.findViewById(R.id.textView2);

            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if(listener != null){
                        int position = getAdapterPosition();
                        if(position != RecyclerView.NO_POSITION){
                            listener.onItemClick(position);
                        }
                    }
                }
            });
        }
    }

    public CartAdapter(ArrayList<Order> itemList){
        mItemList = itemList;

    }



    @NonNull
    @Override
    public Item_TopUp_History_ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_cart,parent,false);
        Item_TopUp_History_ViewHolder vh = new Item_TopUp_History_ViewHolder(v,mListener);
        return vh;
    }

    @Override
    public void onBindViewHolder(@NonNull Item_TopUp_History_ViewHolder holder, int position) {
        Order currentItem= mItemList.get(position);

        //holder.mImageView.setImageResource(currentItem.getmImageResource());
        holder.mTextView1.setText(currentItem.getmText1());
        holder.mTextView2.setText(currentItem.getmText2());
    }


    @Override
    public int getItemCount() {
        return mItemList.size();
    }

}
