package com.iko.android.wallet;


import android.graphics.Bitmap;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;

import com.google.gson.Gson;
import com.google.zxing.BarcodeFormat;
import com.google.zxing.MultiFormatWriter;
import com.google.zxing.common.BitMatrix;
import com.journeyapps.barcodescanner.BarcodeEncoder;


/**
 * A simple {@link Fragment} subclass.
 */
public class GenerateUFragment extends Fragment {

    private EditText etInput,etInput2;
    private Button btnCreatQr;
    private ImageView imageView;
    public GenerateUFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View rootView = inflater.inflate(R.layout.fragment_generate_u, container, false);

        etInput = rootView.findViewById(R.id.editTextQr);
        etInput2 = rootView.findViewById(R.id.editTextQr2);
        btnCreatQr = rootView.findViewById(R.id.buttonQr);
        imageView = rootView.findViewById(R.id.imageViewQr);

        btnCreatQr.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                {
                    String text = etInput.getText().toString().trim();
                    String text2 = etInput2.getText().toString().trim();
                    if(text2.equals("") || text2==null){text2 = "0";}
                    Integer nominal = Integer.valueOf(text2);
                    GeneratorUser user1 = new GeneratorUser(1,text,nominal);
                    Gson gson = new Gson();
                    String json = gson.toJson(user1);



                    if(json !=null){

                        try {
                            MultiFormatWriter multiFormatWriter = new MultiFormatWriter();
                            BitMatrix bitMatrix = multiFormatWriter.encode(json,BarcodeFormat.QR_CODE, 1000, 1000);
                            BarcodeEncoder barcodeEncoder = new BarcodeEncoder();
                            Bitmap bitmap = barcodeEncoder.createBitmap(bitMatrix);
                            imageView.setImageBitmap(bitmap);
                            Log.d("JsonQr",json);
                        }
                        catch (Exception e)
                        {
                            e.printStackTrace();
                        }
                    }

                }
            }
        });


        // Inflate the layout for this fragment
        return rootView;
    }



}
