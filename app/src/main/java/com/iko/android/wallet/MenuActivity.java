package com.iko.android.wallet;

import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.design.widget.BottomNavigationView;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.Toast;

import com.google.zxing.Result;
import com.google.zxing.integration.android.IntentIntegrator;
import com.google.zxing.integration.android.IntentResult;

import java.util.Scanner;

import me.dm7.barcodescanner.zxing.ZXingScannerView;

public class MenuActivity extends AppCompatActivity implements ZXingScannerView.ResultHandler {
    private ZXingScannerView zXingScannerView;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_menu);

        BottomNavigationView bottomNav = findViewById(R.id.bottom_navigation);
        bottomNav.setOnNavigationItemSelectedListener(navigationItemSelectedListener);


        getSupportFragmentManager().beginTransaction().replace(R.id.fragment_container,new HomeFragment()).commit();
    }



    private BottomNavigationView.OnNavigationItemSelectedListener navigationItemSelectedListener =
            new BottomNavigationView.OnNavigationItemSelectedListener() {
                @Override
                public boolean onNavigationItemSelected(@NonNull MenuItem item) {
                    Fragment selectedFragment = null;

                    switch (item.getItemId()){
                        case R.id.nav_home:
                        selectedFragment = new HomeFragment();
                        break;

                        case R.id.nav_history:
                            selectedFragment = new HistoryFragment();
                            break;

                        case R.id.nav_menu:
                            selectedFragment = new MenuFragment();
                            break;

                       /*case R.id.nav_scan:
                            selectedFragment = new ScanFragment();
                            break;*/

                        case R.id.nav_topup:
                            selectedFragment = new TopupFragment();
                            break;
                    }

                    getSupportFragmentManager().beginTransaction().replace(R.id.fragment_container,selectedFragment).commit();
                    return true;
                }
            };


    public void generate(MenuItem item) {
        Intent intentGenerate =new  Intent(this, GenerateActivity.class);
        startActivity(intentGenerate);

    }
    public void send(MenuItem item){
        Intent intentSend =new  Intent(this, SendActivity.class);
        startActivity(intentSend);

    }

    public void message(MenuItem item){
        Intent intentMessage =new  Intent(this, MessageActivity.class);
        startActivity(intentMessage);

    }

    public void setting(MenuItem item){
        Intent intentSetting =new  Intent(this, SettingActivity.class);
        startActivity(intentSetting);

    }
    public void scan(MenuItem item){
        zXingScannerView = new ZXingScannerView(getApplicationContext());
        setContentView(zXingScannerView);
        zXingScannerView.setResultHandler(this);
        zXingScannerView.startCamera();
    }

   /* protected void onPause(){
        super.onPause();
        zXingScannerView.stopCamera();
    }*/

    @Override
    public void onBackPressed() {
        Intent intent = new Intent(this,MenuActivity.class);
        startActivity(intent);
    }

    @Override
    public void handleResult(Result result) {
        Toast.makeText(getApplicationContext(),result.getText(),Toast.LENGTH_SHORT).show();
        zXingScannerView.resumeCameraPreview(this);
        zXingScannerView.stopCamera();
        Intent intent = new Intent(this,PinActivity.class);
        startActivity(intent);


    }




}
