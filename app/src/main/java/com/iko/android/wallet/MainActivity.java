package com.iko.android.wallet;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;

public class MainActivity extends AppCompatActivity {

    public String EXTRA_MESSAGE =null ;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);



    }

    public void login(View view) {
        Intent intentMenu =new  Intent(this, MenuActivity.class);
        EditText username = findViewById(R.id.editText);
        String usernameText = username.getText().toString();
        intentMenu.putExtra(EXTRA_MESSAGE, usernameText);
        startActivity(intentMenu);
        finish();
    }

    public void singup(View view){
        Intent intentRegister =new  Intent(this, RegisterActivity.class);
        startActivity(intentRegister);
        finish();


    }
}
