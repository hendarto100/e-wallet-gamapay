package com.iko.android.wallet;

public class GeneratorUser {
    private Integer idUser;
    private String keterangan;
    private Integer nominal;

    public GeneratorUser(Integer idUser, String keterangan, Integer nominal) {
        this.idUser = idUser;
        this.keterangan = keterangan;
        this.nominal = nominal;
    }

    public Integer getIdUser() {
        return idUser;
    }

    public void setIdUser(Integer idUser) {
        this.idUser = idUser;
    }

    public String getKeterangan() {
        return keterangan;
    }

    public void setKeterangan(String keterangan) {
        this.keterangan = keterangan;
    }

    public Integer getNominal() {
        return nominal;
    }

    public void setNominal(Integer nominal) {
        this.nominal = nominal;
    }
}
