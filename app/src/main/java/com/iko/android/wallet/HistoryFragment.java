package com.iko.android.wallet;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.ArrayList;


/**
 * A simple {@link Fragment} subclass.
 */
public class HistoryFragment extends Fragment {
    private RecyclerView mRecyclerView;
    private RecyclerView.Adapter mAdapter;
    private RecyclerView.LayoutManager mLayoutManager;

    public HistoryFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_history, container, false);


        ArrayList<ExampleItem> itemList = new ArrayList<>();
        itemList.add(new ExampleItem(R.drawable.ic_restaurant_menu_black_24dp,"Kantin Sv","14-10-2018 : 14:23 WIB         Rp.10.000,-"));
        itemList.add(new ExampleItem(R.drawable.ic_restaurant_menu_black_24dp,"Kajur","14-10-2018 : 14:53 WIB         Rp.14.000,-"));
        itemList.add(new ExampleItem(R.drawable.ic_restaurant_menu_black_24dp,"Kafetaria","15-10-2018 : 17:53 WIB         Rp.21.000,-"));
        itemList.add(new ExampleItem(R.drawable.ic_restaurant_menu_black_24dp,"Kafetaria","16-10-2018 : 18:23 WIB         Rp.16.000,-"));
        itemList.add(new ExampleItem(R.drawable.ic_restaurant_menu_black_24dp,"Food Cort","17-10-2018 : 20:13 WIB         Rp.19.000,-"));
        itemList.add(new ExampleItem(R.drawable.ic_restaurant_menu_black_24dp,"Kajur","18-10-2018 : 16:33 WIB         Rp.12.000,-"));

        mRecyclerView = (RecyclerView) rootView.findViewById(R.id.recyclerViewHistory);
        mRecyclerView.setHasFixedSize(true);

        mLayoutManager = new LinearLayoutManager(getActivity());
        mAdapter = new ExampleAdapter(itemList);

        mRecyclerView.setLayoutManager(mLayoutManager);
        mRecyclerView.setAdapter(mAdapter);
        mAdapter.notifyDataSetChanged();


        mRecyclerView.setItemAnimator(new DefaultItemAnimator());


        Log.d("tes",itemList.get(2).getmText1());


        return rootView;
    }

}
