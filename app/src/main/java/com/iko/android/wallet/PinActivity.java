package com.iko.android.wallet;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.Toast;

import com.goodiebag.pinview.Pinview;

import java.util.ArrayList;

public class PinActivity extends AppCompatActivity {
    private Integer count = 0;
    @Override
    protected void onCreate(Bundle savedInstanceState) {

       final ArrayList<Order> orderList = (ArrayList <Order>) getIntent().getSerializableExtra("orderList");

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_pin);
        final Intent intentHome = new Intent(this,MenuActivity.class);
        Pinview pinview = findViewById(R.id.pinView);
        pinview.setPinViewEventListener(new Pinview.PinViewEventListener() {
            @Override
            public void onDataEntered(Pinview pinview, boolean fromUser) {
                Integer pin = Integer.parseInt(pinview.getValue());
                if( pin == 1234){
                startActivity(intentHome);
                    Toast.makeText(PinActivity.this, "Isi "+orderList.get(0).getmText1(),Toast.LENGTH_SHORT).show();
                }
                else{
                    count++;
                    Toast.makeText(PinActivity.this, "Pin Salah Percobaan ke "+count+" dari 5", Toast.LENGTH_SHORT).show();
                }
            }
        });
    }
}
