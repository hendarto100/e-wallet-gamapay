package com.iko.android.wallet;


import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.goodiebag.pinview.Pinview;
import com.google.zxing.Result;
import com.google.zxing.integration.android.IntentIntegrator;

import java.util.ArrayList;

import me.dm7.barcodescanner.zxing.ZXingScannerView;


/**
 * A simple {@link Fragment} subclass.
 */
public class GenerateMFragment extends Fragment {
    private RecyclerView mRecyclerView;
    private RecyclerView mRecyclerViewCart;
    private ExampleAdapter mAdapter;
    private CartAdapter mCartAdapter;
    private RecyclerView.LayoutManager mLayoutManager;
    private RecyclerView.RecyclerListener mListener;
    private ZXingScannerView zXingScannerView;
    private Button mScanOrder;
    private TextView mTotal;
    private int total = 0;
    public GenerateMFragment() {
        // Required empty public constructor
    }



    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        final View rootView = inflater.inflate(R.layout.fragment_generate_m, container, false);

         mScanOrder = rootView.findViewById(R.id.scanOrder);
         mTotal = rootView.findViewById(R.id.totalText);


        final ArrayList<ExampleItem> itemList = new ArrayList<>();
        final ArrayList<Order> orderList = new ArrayList<>();

        itemList.add(new ExampleItem(R.drawable.ic_restaurant_menu_black_24dp,"Nasi Kucing","2000"));
        itemList.add(new ExampleItem(R.drawable.ic_restaurant_menu_black_24dp,"Ikan Pindang","4000"));
        itemList.add(new ExampleItem(R.drawable.ic_restaurant_menu_black_24dp,"Telur Asin","3000"));
        itemList.add(new ExampleItem(R.drawable.ic_restaurant_menu_black_24dp,"Es Jeruk","3000"));
        itemList.add(new ExampleItem(R.drawable.ic_restaurant_menu_black_24dp,"Es Teh","2000"));
        itemList.add(new ExampleItem(R.drawable.ic_restaurant_menu_black_24dp,"Telur Puyuh","2000"));

        //orderList.add(new Order(R.drawable.ic_restaurant_menu_black_24dp,"Telur Puyuh","Rp. 2000"));


        mRecyclerView = (RecyclerView) rootView.findViewById(R.id.recyclerViewMerchant);
        mRecyclerView.setHasFixedSize(true);

        mLayoutManager = new LinearLayoutManager(getActivity());
        mAdapter = new ExampleAdapter(itemList);

        mRecyclerView.setLayoutManager(mLayoutManager);
        mRecyclerView.setAdapter(mAdapter);
        mAdapter.notifyDataSetChanged();

        mAdapter.setOnItemClickListener(new ExampleAdapter.OnItemClickListener() {
            @Override
            public void onItemClick(int position) {


               orderList.add(new Order(itemList.get(position).getmText1(),itemList.get(position).getmText2()));
                //itemList.get(position).setmText1("Ordered");
                mAdapter.notifyItemChanged(position);
                Log.d("tes",orderList.get(0).getmText1());

                int cart = Integer.valueOf(itemList.get(position).getmText2());
                total = total + cart;
                String totalString = String.valueOf(total);
                mTotal.setText("Jumlah yang harus dibayarkan : Rp . " + totalString);
                Log.d("tes",""+total);
                Log.d("tes",""+totalString);

//                int total = 0;
//                try{int harga =Integer.valueOf(itemList.get(position).getmText2());total = harga +total;mTotal.setText(total);}catch (Exception e){mTotal.setText(total);}
//                Log.d("tes",""+total);

                mRecyclerViewCart = (RecyclerView) rootView.findViewById(R.id.recyclerViewMerchantOrder);
                mRecyclerViewCart.setHasFixedSize(true);

                mLayoutManager = new LinearLayoutManager(getActivity(),LinearLayoutManager.HORIZONTAL,false);
                mCartAdapter = new CartAdapter(orderList);


                mRecyclerViewCart.setLayoutManager(mLayoutManager);
                mRecyclerViewCart.setAdapter(mCartAdapter);
                mRecyclerViewCart.smoothScrollToPosition(mRecyclerViewCart.getAdapter().getItemCount()-1);

                mCartAdapter.notifyDataSetChanged();
                mRecyclerViewCart.setItemAnimator(new DefaultItemAnimator());

                mCartAdapter.setOnItemClickListener(new CartAdapter.OnItemClickListener() {
                    @Override
                    public void onItemClick(int position) {
                        int delete = Integer.valueOf(orderList.get(position).getmText2());
                        total = total - delete;

                        String totalString = String.valueOf(total);
                        mTotal.setText("Jumlah yang harus dibayarkan : Rp . " + totalString);

                        orderList.remove(position);
                        mCartAdapter.notifyDataSetChanged();

                    }
                });
                //mRecyclerViewCart.smoothScrollToPosition(position);
            }
        });
///////////////////////////////////////////////////////////////////////////////////////////////////////////

        mScanOrder.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {
                    orderList.get(0).getmText1();
                    //new IntentIntegrator(getActivity()).initiateScan();
                    zXingScannerView = new ZXingScannerView(getActivity());
                    getActivity().setContentView(zXingScannerView);

                    zXingScannerView.startCamera();

                    zXingScannerView.setResultHandler(new ZXingScannerView.ResultHandler() {
                        @Override
                        public void handleResult(Result result) {
                            Toast.makeText(getActivity(), "Contents = " + result.getText() + ", Format = " + result.getBarcodeFormat().toString(), Toast.LENGTH_SHORT).show();
                            zXingScannerView.resumeCameraPreview(this);
                            zXingScannerView.stopCamera();
                            Intent intent = new Intent(getActivity(), PinActivity.class);
                            intent.putExtra("orderList", orderList);
                            startActivity(intent);
                        }
                    });


                }
                catch (Exception e){Toast.makeText(getActivity(), "Masukan Order", Toast.LENGTH_SHORT).show();}
            }
        });


        return rootView;


    }

}
